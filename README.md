# Loan Management

For Mini Aspire Project/Task
(https://aspire-cap.com/)

Laravel Version 5.5 (LTS)
with the use of Passport API

Step 1: Clone the repository. Update the DB details on .env file.
Step 2: "composer update" command to re-check the dependencies
Step 3: "php artisan migrate" to update the database with relevant tables. "php artisan passport:install" command to generate oAuth keys.
OR
Step 3: Upload the database with sample data - aspire_cap_02_12_2018.sql
Step 4: "php artisan serv" command to run the laravel application.

APIs available:

Normal POST API
	1. login
	2. register
Passport API with authorization access token
	3. userDetails
	4. createLoan
	5. rePayment
	
API reference given on: loan_mgt_apis.json

Please let me know, if any challenges.