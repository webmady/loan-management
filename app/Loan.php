<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Loan extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'amount', 'duration', 'repayment_frequency', 'interest_rate', 'arrangement_fee'
    ];

}
