<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use Validator;
use App\Loan;
use App\Repayment;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class FinanceController extends Controller {
	/** 
     * Register api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function createLoan(Request $request) {
        $validator = Validator::make($request->all(), [
            //'user_id' => 'required|numeric', 
            'amount' => 'required|numeric|between:5000,100000', 
            'duration' => 'required|numeric|between:1,6',
            'repayment_frequency' => 'required|numeric|between:1,6'
        ]);
		if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);
        }
		$input = $request->all(); 
		$user = Auth::user();
		$input['user_id'] = $user->id;

		// RAW Query
		$loans = DB::table('loans')
            ->join('repayments', 'repayments.loan_id', '=', 'loans.id')
            //->select('loans.id')
            ->where('loans.user_id', $input['user_id'])
            ->where('repayments.repayment_status', 0)
            ->groupBy('loans.id')
            //->get();
            ->first(['loans.id']);
		if(isset($loans) && $loans->id != '') {
			$failure['message'] = 'Already there is a pending loan Id :'.$loans->id;
			return response()->json(['failure'=>$failure]);
		} else {
			$input['interest_rate'] = 1.5; // SGD
			$input['arrangement_fee'] = 100; // SGD
			$loan = Loan::create($input);
			$interestAmt = ( $input['amount'] * $input['interest_rate'] ) / 100;
			$grossAmt = $input['amount'] + $interestAmt;
			$amt = $grossAmt / $input['repayment_frequency'];
			$payment = array();
			for($i=0;$i<$input['repayment_frequency'];$i++){
				if($i==0) { // First month
					$netAmt = $grossAmt + $input['arrangement_fee'];
					$payment[$i]['remarks'] = "Inclusive of arrangement fee SGD ".$input['arrangement_fee']." for first payment. Common interest rate : ".$input['interest_rate'];
				} else {
					$netAmt = $grossAmt;
				}
				$payment[$i]['amount'] = $netAmt;
				$payment[$i]['repayment_frequency'] = $i+1;
				$payment[$i]['loan_id'] = $loan->id;
				$repayment = Repayment::create($payment[$i]);
				unset($payment[$i]['loan_id']);
			}
			//$success['token'] =  $user->createToken('MyApp')-> accessToken; 
			$success['loan_id'] =  $loan->id;
			$success['repayment_details'] =  $payment;
			$success['message'] =  'Loan created successfully';
			return response()->json(['success'=>$success]);
		}
    }

    public function rePayment(Request $request) {
        $validator = Validator::make($request->all(), [
            'loan_id' => 'required|numeric',
            'repayment_frequency' => 'required|numeric|between:1,6'
        ]);
		if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        }
		$input = $request->all();
		$user = Auth::user();
		$input['user_id'] = $user->id;
		$rePaymentId = '';
		$failure['message'] = 'Failed';
		$loanArr = Loan::where(array('id'=>$input['loan_id'] , 'user_id' => $input['user_id']))->first(['id']);
		if(isset($loanArr) && $loanArr->id == $input['loan_id']) {
			$paymentArr = Repayment::where(array('loan_id'=>$input['loan_id']))->get();
			foreach($paymentArr as $key => $val) {
				if($val->repayment_status == 0 && $input['repayment_frequency'] == $val->repayment_frequency) {
					$rePaymentId = $val->id;
					break;
				} else if($val->repayment_status == 0  && $input['repayment_frequency'] > $val->repayment_frequency) {
					$failure['message'] = "Previous repayment frequency (".$val->repayment_frequency.") has not yet paid.";
					break;
				} else if($val->repayment_status == 1  && $input['repayment_frequency'] == $val->repayment_frequency) {
					$failure['message'] = "Already paid on ".$val->repayment_date;
					break;
				}
			}
		} else {
			$failure['message'] = "Invalid loan id";
		}
		if($rePaymentId != '') {
			$loanArr = Repayment::where('id',$rePaymentId)->update(array('repayment_status'=>'1','repayment_date'=>Carbon::now()));
			$success['loan_id'] =  $input['loan_id'];
			$success['repayment_frequency'] =  $input['repayment_frequency'];
			$success['message'] =  'Repayment updated successfully';
			return response()->json(['success'=>$success]);
		} else {
			return response()->json(['failure'=>$failure]);
		}
    }

}
